import numpy as np
import pandas as pd
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from tensorflow.keras.layers import Embedding,Dense,LSTM,SpatialDropout1D
import tensorflow
import seaborn as sns
import matplotlib.patches as mpatches
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix,classification_report,accuracy_score,f1_score,precision_score,recall_score
from tensorflow.keras.models import load_model
import matplotlib.pyplot as plt
from numpy import asarray
from numpy import zeros
import tensorflow as tf
import pickle
import codecs
from sklearn.metrics import accuracy_score,f1_score,precision_score,recall_score
from nltk.tokenize import word_tokenize
import gensim.models as Word2vec
import sqlite3
from sklearn.model_selection import KFold

PROCESSED_DATA_PATH = 'data/hasil'
# train_data = pd.read_csv('{}/data_trainbaru.csv'.format(PROCESSED_DATA_PATH))
# train_data = train_data[['hasil_normalisasi', 'Label']]

connection=sqlite3.connect('data/Admin.db')
train_data=pd.read_sql_query('select * from dataset',connection)
connection.close()

class LSTM_model:

    def __init__(self,filepath=PROCESSED_DATA_PATH):
        self.max_fatures = 5000
        self.batch_size = 128
        # self.build_model()
        return print('Train data shape :', train_data.shape)

    def describe_model(self):
        bullying = 0
        netral = 0
        nonbull = 0
        irrelevant = 0
        for label in train_data['Label']:
            if label == 0:
                bullying += 1
            elif label == 1:
                irrelevant += 1
            elif label == 2:
                netral += 1
            else:
                nonbull += 1

        print("Total komentar cyberbullying adalah " + str(bullying) + ", atau",
              '{0:.2%}'.format(bullying / len(train_data['Label'])))
        print("Total komentar Non cyberbullying adalah " + str(irrelevant) + ", atau ",
              '{0:.2%}'.format(irrelevant / len(train_data['Label'])))
        print("Total komentar Netral adalah " + str(netral) + ", atau ",
              '{0:.2%}'.format(netral / len(train_data['Label'])))
        print("Total komentar Non cyberbullying adalah " + str(nonbull) + ", atau ",
              '{0:.2%}'.format(nonbull / len(train_data['Label'])))
        missing_train = train_data.isnull().sum()
        print(missing_train)
        # n_words = [len(komen) for komen in train_data.hasil_normalisasi]
        # n_words = pd.Series(n_words)
        # n_words.describe()
        # print(train_data['hasil_normalisasi'].describe())
        x = train_data.Label.value_counts()
        sns.barplot(x.index, x,)
        plt.gca().set_ylabel('Jumlah')
        plt.gca().set_xlabel('Score')
        plt.title("Distribusi Data")
        red_patch = mpatches.Patch(color='red', label='Non Cyberbullying')
        blue_patch = mpatches.Patch(color='blue', label='Cyberbullying')
        orange_patch = mpatches.Patch(color='orange', label='Irrelevant')
        green_patch = mpatches.Patch(color='green', label='Netral')
        plt.legend(handles=[blue_patch,orange_patch,green_patch,red_patch],bbox_to_anchor=(1.05, 1.0), loc='upper left')
        plt.tight_layout()
        # plt.legend(['Cyberbullying', 'Irrelevant','Netral','Non Cyberbullying'], loc='lower right')
        plt.savefig('static/plot_image/distribusi.png')

    def tokenize_data(self):
        self.tokenizer = Tokenizer(num_words=self.max_fatures, split=' ')
        self.tokenizer.fit_on_texts(train_data['hasil_normalisasi'].values)
        self.X = self.tokenizer.texts_to_sequences(train_data['hasil_normalisasi'].values)
        print(self.X)
        self.X = pad_sequences(self.X)
        # print(X[:2])
        self.Y = pd.get_dummies(train_data['Label']).values
        print('Nilai Y adalah ', self.Y)
        self.X_train, self.X_test, self.Y_train, self.Y_test = train_test_split(self.X, self.Y, test_size=0.20,
                                                                                random_state=42,shuffle=True)
        print(self.X_train.shape,self.Y_train.shape)
        print(self.X_test.shape, self.Y_test.shape)
        with open('data/tokenizer.pickle', 'wb') as handle:
            pickle.dump(self.tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
        self.vocab_size = len(self.tokenizer.word_index) + 1

    def word2vec(self):
        with codecs.open('clear_traincoba.csv', 'r')as f:
            for line in f:
                tweet = f.readlines()
                tokenized_sent = [word_tokenize(i) for i in tweet]
                # for i in tokenized_sent:
                #     print(i)
        model = Word2vec.Word2Vec(tokenized_sent, min_count=1, size=200,window=1,sg=0)
        print(model)
        model.wv.save_word2vec_format('model/model_word2vecWindow.txt', binary=False)
        word = list(model.wv.vocab)

    def make_model(self):
        embeddings_index = dict()
        # f = open('mymodel/idwiki_word2vec_200.model',encoding='utf-8')
        f = open('model/model_word2vecWindow.txt')
        for line in f:
            values = line.split()
            word = values[0]
            coefs = asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs
        f.close()
        print('Loaded %s word vectors.' % len(embeddings_index))

        # create a weight matrix for words in training docs
        embedding_matrix = zeros((self.vocab_size, 200))
        for word, i in self.tokenizer.word_index.items():
            embedding_vector = embeddings_index.get(word)
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector

        embed_dim = 200
        lstm_out = 128
        model = tensorflow.keras.Sequential()
        print("rincian:")
        print(self.vocab_size)
        print(embed_dim)
        print(self.X.shape[1])
        model.add(Embedding(self.vocab_size, embed_dim,weights=[embedding_matrix],input_length=self.X.shape[1]))
        model.add(SpatialDropout1D(0.4))
        model.add(LSTM(lstm_out, dropout=0.5, recurrent_dropout=0.5))
        # model.add(LSTM(lstm_out,activation='tanh'))
        model.add(Dense(4, activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        print(model.summary())
        history=model.fit(self.X_train, self.Y_train, epochs=20, batch_size=self.batch_size, validation_data=(
            self.X_test, self.Y_test), verbose=1)
        model.save('model/My_model.h5')
        # hist_df=pd.DataFrame(self.history.history)
        # with open('model/history.json','w')as file:
        #     json.dump(history.history,file)
        np.save('model/history_model.npy',history.history)

    def load_model(self):
        self.model = load_model('model/My_model.h5')
        Y_pred = self.model.predict_classes(self.X_test, batch_size=self.batch_size)
        df_test = pd.DataFrame({'true': self.Y_test.tolist(), 'pred': Y_pred})
        df_test['true'] = df_test['true'].apply(lambda x: np.argmax(x))
        print("confusion matrix")
        print(confusion_matrix(df_test.true, df_test.pred))
        print(classification_report(df_test.true, df_test.pred))

    def show_plot(self):
        # with open('model/history.json','r') as f:
        #     history_json=json.loads(f.read())
        history=np.load('model/history_model.npy',allow_pickle='TRUE').item()
        plt.figure(figsize=(12, 12))
        plt.plot(history['loss'])
        plt.plot(history['val_loss'])
        plt.title('Loss')
        plt.legend(['train', 'val'], loc='upper left')
        # plt.show()
        plt.savefig('static/plot_image/loss.png')
        plt.clf()

        plt.figure(figsize=(12, 12))
        plt.plot(history['accuracy'])
        plt.plot(history['val_accuracy'])
        plt.title('Accuracy')
        plt.legend(['train', 'val'], loc='upper left')
        # plt.show()
        plt.savefig('static/plot_image/akurasi.png')
        plt.clf()

    def Kfold_cross(self):
        embeddings_index = dict()
        # f = open('mymodel/idwiki_word2vec_200.model',encoding='utf-8')
        f = open('model/model_word2vecWindow.txt')
        for line in f:
            values = line.split()
            word = values[0]
            coefs = asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs
        f.close()
        print('Loaded %s word vectors.' % len(embeddings_index))

        # create a weight matrix for words in training docs
        embedding_matrix = zeros((self.vocab_size, 200))
        for word, i in self.tokenizer.word_index.items():
            embedding_vector = embeddings_index.get(word)
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector
        n_split = 10
        i = 0
        batch_size = 128
        accuracy_model = []
        f1_model = []
        precission_model = []
        recall_model = []
        for train_index, test_index in KFold(n_split, shuffle=True).split(self.X):
            i = i + 1
            x_train, x_test = self.X[train_index], self.X[test_index]
            y_train, y_test = self.Y[train_index], self.Y[test_index]
            model = tf.keras.Sequential()
            model.add(Embedding(self.vocab_size, 200, weights=[embedding_matrix], input_length=self.X.shape[1],
                                trainable=True))
            # model.add(embedding_layer)1
            # model.add(SpatialDropout1D(0.4))
            # model.add(Dropout(0.5))
            model.add(LSTM(128, dropout=0.5, recurrent_dropout=0.5))
            model.add(Dense(4, activation='softmax'))
            model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
            print(model.summary())
            model.fit(x_train, y_train, epochs=20)
            # print('Model evaluation ',model.evaluate(x_test,y_test))
            accuracy_model.append(accuracy_score(y_test, np.round(model.predict(x_test)), normalize=True) * 100)
            f1_model.append(f1_score(y_test, np.round(model.predict(x_test)), average='weighted') * 100)
            recall_model.append(recall_score(y_test, np.round(model.predict(x_test)), average='weighted') * 100)
            precission_model.append(precision_score(y_test, np.round(model.predict(x_test)), average='weighted') * 100)
            model.save('kfold/my_model{}.h5'.format(i))
            Y_pred = model.predict_classes(x_test, batch_size=batch_size)
            df_test = pd.DataFrame({'true': y_test.tolist(), 'pred': Y_pred})
            df_test['true'] = df_test['true'].apply(lambda x: np.argmax(x))
            print("confusion matrix", confusion_matrix(df_test.true, df_test.pred))
            data=confusion_matrix(df_test.true, df_test.pred)
            svm = sns.heatmap(data, annot=True, cmap='coolwarm', linecolor='white', linewidths=1)
            svm.get_figure()
            plt.savefig('static/kfold/kfold{}.png'.format(i), dpi=400)
            plt.clf()

        barWidth = 0.25
        accuracy = np.arange(len(accuracy_model))
        recall = [x + barWidth for x in accuracy]
        precission = [x + barWidth for x in recall]
        f1 = [x + barWidth for x in precission]

        plt.bar(accuracy, accuracy_model, color='blue', width=barWidth, edgecolor='white', label='Accuracy')
        plt.bar(recall, recall_model, color='green', width=barWidth, edgecolor='white', label='Recall')
        plt.bar(precission, precission_model, color='orange', width=barWidth, edgecolor='white', label='Precission')
        plt.bar(f1, f1_model, color='red', width=barWidth, edgecolor='white', label='F1 Score')
        K=['K1', 'K2', 'K3', 'K4', 'K5','K6','K7','K8','K9','K10']
        plt.xlabel('group', fontweight='bold')
        plt.xticks([r + barWidth for r in range(len(accuracy_model))],
                   K)

        plt.legend()
        plt.title('Cross validation 10 Fold dengan 50 Epoch')
        # plt.show()
        plt.savefig('static/plot_image/kfold2.png')

        average_accuracy = sum(accuracy_model) / len(accuracy_model)
        F1_average = sum(f1_model) / len(f1_model)
        precission_average = sum(precission_model) / len(precission_model)
        recall_average = sum(recall_model) / len(recall_model)

        df = pd.DataFrame({'Fold': K,'Accuracy':accuracy_model,'F1-Score':f1_model,'Presisi':precission_model,
                           'Recall':recall_model})
        print(df)
        df.to_csv('kfold.csv',index=False)
        print("Rata-rata akurasi sebesar {}".format(average_accuracy))
        print("Rata-rata F1-Score sebesar {}".format(F1_average))
        print("Rata-rata Precission sebesar {}".format(precission_average))
        print("Rata-rata Recall sebesar {}".format(recall_average))

    def new_model(self):
        self.describe_model()
        self.tokenize_data()
        self.word2vec()
        self.make_model()
        self.load_model()
        self.show_plot()
        # self.Kfold_cross()

    def build_model(self):
        self.describe_model()
        self.tokenize_data()
        # self.make_model()
        self.load_model()
        self.show_plot()

    def predict_comment(self,text):
        # self.build_model()
        self.model = load_model('model/My_model.h5')
        with open('data/tokenizer.pickle', 'rb') as handle:
            tokenizer2 = pickle.load(handle)
        twt=[]
        twt.append(text)
        print(twt)
        # vectorizing the tweet by the pre-fitted tokenizer instance
        twt = tokenizer2.texts_to_sequences(twt)
        # padding the tweet to have exactly the same shape as `embedding_2` input
        twt = pad_sequences(twt, maxlen=110, dtype='int32', value=0)
        print(twt)

        sentiment = self.model.predict(twt,batch_size=1, verbose=2)[0]
        kelas=self.model.predict_classes(twt,batch_size=1)
        print(kelas)
        print(sentiment)
        if (kelas == [0]):
            print("Cyberbullying")
        elif (kelas == [1]):
            print("Irrelevant")
        elif (kelas == [2]):
            print("Netral")
        else:
            print("Non Cyberbullying")

        dict={}
        dict['class']=kelas
        dict['probabilities']=sentiment
        # print(dict['class'])
        # print(dict['probabilities'][1])
        return dict

    def balas_komen(self,text):
        import Prepocessing_sentence as normal
        self.model = load_model('model/My_model.h5')
        with open('data/tokenizer.pickle', 'rb') as handle:
            tokenizer2 = pickle.load(handle)
        text=normal.text_prepocessing(text)
        twt=[]
        twt.append(text)
        print(twt)
        # vectorizing the tweet by the pre-fitted tokenizer instance
        twt = tokenizer2.texts_to_sequences(twt)
        # padding the tweet to have exactly the same shape as `embedding_2` input
        twt = pad_sequences(twt, maxlen=110, dtype='int32', value=0)
        print(twt)
        sentiment = self.model.predict(twt,batch_size=1, verbose=2)[0]
        kelas=self.model.predict_classes(twt,batch_size=1)
        print(kelas)
        print(sentiment)
        if (kelas == [0]):
            kelas=0
        elif (kelas == [1]):
            kelas=1
        elif (kelas == [2]):
            kelas=2
        else:
            kelas=3
        return kelas

    def prediction(self,array):
        import Prepocessing_sentence as normal
        data=[]
        for i in range(len(array)):
            clear=normal.text_prepocessing(array[i])
            data.append(clear)
        self.model = load_model('model/My_model.h5')
        with open('data/tokenizer.pickle', 'rb') as handle:
            tokenizer2 = pickle.load(handle)
        # vectorizing the tweet by the pre-fitted tokenizer instance
        twt = tokenizer2.texts_to_sequences(data)
        # padding the tweet to have exactly the same shape as `embedding_2` input
        twt = pad_sequences(twt, maxlen=110, dtype='int32', value=0)
        print(twt)

        sentiment = self.model.predict(twt, batch_size=1, verbose=2)[0]
        kelas = self.model.predict_classes(twt, batch_size=1)
        return kelas

# test=LSTM_model()
# test.build_model()
# test.predict_comment('ada yang bulat tapi bukan tekat')
