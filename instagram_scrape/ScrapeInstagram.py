from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException,TimeoutException
import time
import re
import urllib.request
import urllib3
from lstm_model import LSTM_model
import os
import chromedriver_binary
from webdriver_manager.chrome import ChromeDriverManager

class go_url:
    def __init__(self):
        self.driver_path()
        return None

    def driver_path(self):
        options = webdriver.ChromeOptions()
    #     options.add_argument("--headless")
        self.driver = webdriver.Chrome(executable_path='chromedriver', options=options)

    def profile_screenshoot(self,username,password,url2):
        url = 'https://www.instagram.com/accounts/login/'
        self.driver.get(url)
        usernameInput = username
        passwordInput = password
        username = WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.XPATH, "//input[@name='username']"))).send_keys(usernameInput)
        password = WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.XPATH, "//input[@name='password']"))).send_keys(passwordInput)
        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.XPATH, "//input[@name='password']"))).send_keys(
            Keys.ENTER)
        time.sleep(15)
        try:
            error="//*[@id='slfErrorAlert']"
            alert=self.driver.find_element(By.XPATH,error)
            self.driver.close()
            return False
        except:
            self.driver.get(url2)
            time.sleep(5)
            self.driver.save_screenshot('static/images/profil.png')
            self.driver.close()
            return True

    def login_page(self,url2,kode,username,password):
        try:
            url='https://www.instagram.com/accounts/login/'
            self.driver.get(url)
            usernameInput = username
            passwordInput = password
            username = WebDriverWait(self.driver, 20).until(
                EC.element_to_be_clickable((By.XPATH, "//input[@name='username']"))).send_keys(usernameInput)
            password = WebDriverWait(self.driver, 20).until(
                EC.element_to_be_clickable((By.XPATH, "//input[@name='password']"))).send_keys(passwordInput)
            WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//input[@name='password']"))).send_keys(
                Keys.ENTER)
            time.sleep(10)
            self.post_page(url2,kode)
            print('login success')
        except TimeoutException:
            return False

    def send_warning(self,id):
        num=id+1
        try:
            try:
                id = "//*[@id='react-root']/section/main/div/div/article/div/div[3]/div[1]/ul/ul[{}]/div/li/div/div[" \
                                         "1]/div[2]/div/div/button[2]".format(num)
                do_klik=self.driver.find_element_by_xpath(id)
                do_klik.click()
            except:
                id = "//*[@id='react-root']/section/main/div/div/article/div/div[3]/div[1]/ul/ul[{}]/div/li/div/div[" \
                                         "1]/div[2]/div/div/button".format(num)
                do_klik = self.driver.find_element_by_xpath(id)
                do_klik.click()
        except:
            try:
                try:
                    id = "//*[@id='react-root']/section/main/div/div/article/div[3]/div[1]/ul/ul[3]/div/li/div/div[1]/div[" \
                         "2]/div/div/button[2]".format(num)
                    wait = WebDriverWait(self.driver, 10)
                    do_klik = wait.until(EC.presence_of_element_located((By.XPATH, id)))
                    do_klik.click()
                except:
                    id = "//*[@id='react-root']/section/main/div/div/article/div[3]/div[1]/ul/ul[{}]/div/li/div/div[1]/div[" \
                         "2]/div/div/button".format(num)
                    wait = WebDriverWait(self.driver, 10)
                    do_klik = wait.until(EC.presence_of_element_located((By.XPATH, id)))
                    do_klik.click()
            except:
                try:
                    id = "//*[@id='react-root']/section/main/div/div[1]/article/div[3]/div[1]/ul/ul[{}]/div/li/div/div[" \
                         "1]/div[2]/div/div/button[2]"
                    wait = WebDriverWait(self.driver, 10)
                    do_klik = wait.until(EC.presence_of_element_located((By.XPATH, id)))
                    do_klik.click()
                except:
                    id = "//*[@id='react-root']/section/main/div/div[1]/article/div[3]/div[1]/ul/ul[{}]/div/li/div/div[" \
                         "1]/div[2]/div/div/button".format(num)
                    wait = WebDriverWait(self.driver, 10)
                    do_klik = wait.until(EC.presence_of_element_located((By.XPATH, id)))
                    do_klik.click()
        peringatan="test"
        element_komen = self.driver.find_element_by_class_name('Ypffh')
        element_komen.send_keys(peringatan)
        # element_komen2 = self.driver.find_element_by_class_name('Ypffh')
        element_komen.send_keys(Keys.ENTER)
        time.sleep(5)


    def post_page(self,url,kode):
        url_post = url
        self.driver.get(url_post)
        print('find function')
        try:
            if (self.driver.find_element_by_xpath(
                    '/html/body/div[1]/section/nav/div[2]/div/div/div[3]/div/div/div/div/div[2]/div[1]/div').is_displayed(
                    True)):
                self.login_page()
        except:
            pass
        try:
            # load_more_comment = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable(
            #     (By.XPATH, '//*[@id="react-root"]/section/main/div/div/article/div[2]/div[1]/ul/li/div/button')))
            load_more_comment = self.driver.find_element_by_xpath(
                '//*[@id="react-root"]/section/main/div/div[1]/article/div[3]/div[1]/ul/li/div/button')
            i = 0
            while load_more_comment or i < 20:
                load_more_comment.click()
                time.sleep(2)
                # load_more_comment = self.driver.find_element_by_xpath(
                #     '//*[@id="react-root"]/section/main/div/div[1]/article/div[3]/div[1]/ul/li/div/button')
                load_more_comment=WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//*[@id='react-root']/section/main/div/div[1]/article/div[3]/div["
                                                  "1]/ul/li/div/button")))
                load_more_comment.click()
                time.sleep(2)
                i += 1
        except:
            pass

        user_names = []
        user_comments = []
        label_comment = []
        tuple = []
        comment = self.driver.find_elements_by_class_name('Mr508')
        i=1
        for c in comment:
            name = self.driver.find_element_by_xpath("//*[@id='react-root']/section/main/div/div/article/div[3]/div["
                                                     "1]/ul/ul[{}]/div/li/div/div[1]/div[2]/h3/div/span/a".format(
                i)).text
            content = self.driver.find_element_by_xpath("//*[@id='react-root']/section/main/div/div/article/div["
                                                        "3]/div[1]/ul/ul[{}]/div/li/div/div[1]/div[2]/span".format(
                i)).text
            content = content.replace('\n', ' ').strip().rstrip()
            user_names.append(name)
            user_comments.append(content)
            i+=1

        x=LSTM_model().prediction(user_comments)
        print(x)
        if kode=='2':
            for id in range(len(x)):
                if x[id]==0:
                    self.send_warning(id)
                else:
                    print("not find")

        username = self.driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/div/article/div[3]/div[1]/ul/div/li/div/div/div[2]/h2/div/span/a').text
        caption = self.driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/div/article/div[3]/div[1]/ul/div/li/div/div/div[2]/span').text
        try:
            likes=self.driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/div/article/div/div[3]/section[2]/div/div/button/span').text
        except:
            try:
                likes = self.driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/div/article/div['
                                                   '3]/section[2]/div/div/button/span').text
            except:
                likes=self.driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/div[1]/article/div['
                                                        '3]/section[2]/div/span/span').text
        label=[]
        for i in range(len(user_comments)):
            if x[i]==0:
                label.append('Cyberbullying')
            elif x[i]==1:
                label.append('Irrelevant')
            elif x[i]==2:
                label.append('Netral')
            else:
                label.append('Bukan Cyberbullying')
        tuple=list(zip(user_names,user_comments,label))
        print(label)
        from instagram_scrape import save
        save.export(url_post, tuple, username, caption, likes)
        text = re.sub(r'[^a-zA-Z0-9]', '', url_post)
        print(text)
        try:
            try:
                try:
                    img = self.driver.find_element_by_xpath(
                        '//*[@id="react-root"]/section/main/div/div/article/div/div[2]/div/div/div[1]/img')
                    src = img.get_attribute('src')
                    urllib.request.urlretrieve(src, 'static/images/' + text + '.png')
                except:
                    img = self.driver.find_element_by_xpath(
                        '//*[@id="react-root"]/section/main/div/div/article/div[2]/div/div/div[1]/img')
                    src = img.get_attribute('src')
                    urllib.request.urlretrieve(src, 'static/images/' + text + '.png')
            except:
                img=self.driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/div[1]/article/div[2]/div/div/div[1]/div[1]/img')
                src = img.get_attribute('src')
                urllib.request.urlretrieve(src, 'static/images/' + text + '.png')
        except:
            try:
                try:
                    img = self.driver.find_element_by_xpath(
                        '//*[@id="react-root"]/section/main/div/div[1]/article/div[1]/div/div/div/div/div/video')
                    src = img.get_attribute('poster')
                    urllib.request.urlretrieve(src, 'static/images/' + text + '.png')
                except:
                    img = self.driver.find_element_by_xpath(
                        '//*[@id="react-root"]/section/main/div/div[1]/article/div[2]/div/div/div[1]/div/div/video')
                    src = img.get_attribute('poster')
                    urllib.request.urlretrieve(src, 'static/images/' + text + '.png')
            except:
                img = self.driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/div['
                                                        '1]/article/div/div[2]/div/div/div[1]/div[1]/img')
                src = img.get_attribute('src')
                urllib.request.urlretrieve(src, 'static/images/' + text + '.png')

            return True
        self.driver.close()


# arif=go_url()
# arif.login_page()
# arif.post_page('https://www.instagram.com/p/CA1c8b4DIFA/')